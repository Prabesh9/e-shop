package com.esapmarka.eshop.message;


import com.esapmarka.eshop.domain.user.User;

// import com.itglance.simple5d.domain.user.role.Role;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDtoResponse {
    private User user;
    private String message;
    private String token;

    public LoginDtoResponse(User user, String message, String token) {
        this.user = user;
        this.message = message;
        this.token = token;
    }

    public LoginDtoResponse() {

    }
}
