package com.esapmarka.eshop.domain.user;

public interface UserService {
    public User authenticate(UserLoginDto userLoginDto);
    public User register(User user);
}
