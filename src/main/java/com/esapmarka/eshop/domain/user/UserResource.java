package com.esapmarka.eshop.domain.user;

import com.esapmarka.eshop.message.LoginDtoResponse;
import com.esapmarka.eshop.message.ResponseMessage;
import com.esapmarka.eshop.security.JwtTokenUtil;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserResource {

    private UserService userService;
	private JwtTokenUtil jwtTokenUtil;

    public UserResource(UserService userService, JwtTokenUtil jwtTokenUtil) {
        this.userService = userService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate(@RequestBody UserLoginDto userLoginDto){
        User user = userService.authenticate(userLoginDto);
        if (user != null) {
            return ResponseEntity.ok().body(new LoginDtoResponse(user, "Login Successfull", jwtTokenUtil.generateToken(userLoginDto.getUsername())));
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ResponseMessage("Invalid Credentials"));
    }
    
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody User user) {
        User registeredUser = userService.register(user);
        return ResponseEntity.ok().body(registeredUser);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/save")
    public String adminCan() {
        return "adminCan";
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/delete")
    public String userCan() {
        return "userCan";
    }

}
