package com.esapmarka.eshop.domain.product;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("SELECT prd FROM Product prd WHERE prd.category.id = :catId")
    List<Product> getProductsOfCategory(@Param("catId") long catId);

    @Query("SELECT prd FROM Product prd WHERE prd.user.id = :userId")
    List<Product> getProductsOfUser(@Param("userId") long userId);

    @Query("SELECT prd FROM Product prd WHERE upper(prd.name) LIKE concat('%', upper(:search), '%')")
    List<Product> getAllProducts(String search, Sort sort);

    @Query(value = "SELECT * FROM product prd ORDER BY prd.view DESC LIMIT 10", nativeQuery = true)
    List<Product> getPopularProducts();

    List<Product> findAllByFeatured(boolean featured);
}
