package com.esapmarka.eshop.domain.product;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

	@Override
	public Product addProduct(Product product) {
		return productRepository.save(product);
	}

	@Override
	public List<Product> listProducts(String orderBy, String limit, boolean ascending, String search) {
        if (ascending)
            return productRepository.getAllProducts(search, Sort.by(orderBy));
        return productRepository.getAllProducts(search, Sort.by(orderBy).descending());
	}

	@Override
	public Optional<Product> productDetail(long prdId) {
		return productRepository.findById(prdId);
	}

	@Override
	public List<Product> listProductsOfCategory(long catId) {
        List<Product> productsOfCategory = productRepository.getProductsOfCategory(catId);
        return productsOfCategory;
	}

	@Override
	public List<Product> listProductsOfUser(long userId) {
		List<Product> productsOfUser = productRepository.getProductsOfUser(userId);
        return productsOfUser;
	}

	@Override
	public boolean existsProduct(long prdId) {
        Optional<Product> product = productDetail(prdId);
        return product.isPresent();
	}

    @Override
    public void increaseProductViews(Product product) {
        product.setView(product.getView()+1);
        productRepository.save(product);
    }

	@Override
	public List<Product> listPopularProducts() {
		return productRepository.getPopularProducts();
	}

	@Override
	public List<Product> toggleProductFromFeaturedList(long prdId, boolean status) {
        Optional<Product> product = productDetail(prdId);
        if (product.isPresent()) {
            Product featuredProduct = product.get();
            featuredProduct.setFeatured(true);
            productRepository.save(featuredProduct);
            return viewFeaturedList();
        }
        return null;
	}

	public List<Product> viewFeaturedList() {
        return productRepository.findAllByFeatured(true);
	}

}
