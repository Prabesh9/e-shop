package com.esapmarka.eshop.domain.product;

import java.util.List;
import java.util.Optional;

import com.esapmarka.eshop.message.ResponseMessage;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/product")
public class ProductResource {
    
    private ProductService productService;

    public ProductResource(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping(value = "/create")
    public ResponseEntity<?> createProduct(@RequestBody Product product) {
        Product createdProduct = productService.addProduct(product);
        return ResponseEntity.ok().body(createdProduct);
    }

    @GetMapping(value = "/")
    public ResponseEntity<?> getAllProduct(
            @RequestParam(required = false, name = "orderBy", defaultValue = "createdAt") String orderBy,
            @RequestParam(required = false, name = "ascending") boolean ascending,
            @RequestParam(required = false, name = "search", defaultValue = "") String search
        ) {
        ascending = (orderBy.equals("price") || ascending) ? true : false;
        return ResponseEntity.ok().body(productService.listProducts(orderBy, "", ascending, search));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getProductById(@PathVariable(name = "id") long prdId) {
        Optional<Product> product = productService.productDetail(prdId);
        if (product.isEmpty())
            return ResponseEntity.badRequest().body(new ResponseMessage("Category of given id cannot be found"));
        Product productViewed = product.get();
        productService.increaseProductViews(productViewed);
        return ResponseEntity.ok().body(productViewed);
    }

    @GetMapping(value = "/category/{id}")
    public ResponseEntity<?> getProductsOfCategory(@PathVariable(name = "id") long catId) {
        List<Product> products = productService.listProductsOfCategory(catId);
        return ResponseEntity.ok().body(products);
    }

    @GetMapping(value = "/user/{id}")
    public ResponseEntity<?> getProductsOfUser(@PathVariable(name = "id") long userId) {
        List<Product> products = productService.listProductsOfUser(userId);
        return ResponseEntity.ok().body(products);
    }

    @GetMapping(value = "/popular")
    public ResponseEntity<?> getPopularProducts() {
        List<Product> products = productService.listPopularProducts();
        return ResponseEntity.ok().body(products);
    }

    @GetMapping(value = "{id}/add-to-featured")
    public ResponseEntity<?> addProductToFeaturedList(@PathVariable(name = "id") long prdId) {
        List<Product> products = productService.toggleProductFromFeaturedList(prdId, true);
        if (products != null)
            return ResponseEntity.ok().body(products);
        return ResponseEntity.badRequest().body(new ResponseMessage("Product with id not found"));
    }

    @GetMapping(value = "{id}/remove-from-featured")
    public ResponseEntity<?> removeProductFromFeaturedList(@PathVariable(name = "id") long prdId) {
        List<Product> products = productService.toggleProductFromFeaturedList(prdId, false);
        if (products != null)
            return ResponseEntity.ok().body(products);
        return ResponseEntity.badRequest().body(new ResponseMessage("Product with id not found"));    
    }

}
