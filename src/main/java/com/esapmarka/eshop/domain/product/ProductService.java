package com.esapmarka.eshop.domain.product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    public Product addProduct(Product product);
    public List<Product> listProducts(String orderBy, String limit, boolean ascending, String search);
    public Optional<Product> productDetail(long prdId);
    public List<Product> listProductsOfCategory(long catId);
    public List<Product> listProductsOfUser(long userId);
	public boolean existsProduct(long prdId);
    public void increaseProductViews(Product product);
    public List<Product> listPopularProducts();
    public List<Product> toggleProductFromFeaturedList(long prdId, boolean status);
    public List<Product> viewFeaturedList();
}
