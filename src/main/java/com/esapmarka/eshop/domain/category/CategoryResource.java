package com.esapmarka.eshop.domain.category;

import java.util.Optional;

import com.esapmarka.eshop.message.ResponseMessage;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/category")
public class CategoryResource {
    
    private CategoryService categoryService;

    public CategoryResource(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping(value = "/create")
    public ResponseEntity<?> createCategory(@RequestBody Category category) {
        Category createdCategory = categoryService.addCategory(category);
        return ResponseEntity.ok().body(createdCategory);
    }

    @GetMapping(value = "/")
    public ResponseEntity<?> getAllCategory() {
        return ResponseEntity.ok().body(categoryService.listCategory());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable(name = "id") long catId) {
        Optional<Category> category = categoryService.categoryDetail(catId);
        if (category.isPresent())
            return ResponseEntity.ok().body(category.get());
        else
            return ResponseEntity.badRequest().body(new ResponseMessage("Category of given id cannot be found"));
    }

    @PutMapping(value = "/update/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable(name = "id") long catId, @RequestBody Category category) {
        if (!categoryService.existsCategory(catId))
            return ResponseEntity.badRequest().body(new ResponseMessage("Category of given id cannot be found"));
        Category updatedCategory = categoryService.updateCategory(category);
        return ResponseEntity.ok().body(updatedCategory);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable(name = "id") long catId) {
        if (!categoryService.existsCategory(catId))
            return ResponseEntity.badRequest().body(new ResponseMessage("Category of given id cannot be found"));
        categoryService.deleteCategory(catId);
        return ResponseEntity.ok().body(new ResponseMessage("Category has been deleted"));
    }

}
