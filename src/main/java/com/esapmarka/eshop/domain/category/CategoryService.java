package com.esapmarka.eshop.domain.category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    public Category addCategory(Category category);
    public List<Category> listCategory();
    public Optional<Category> categoryDetail(long catId);
    public Category updateCategory(Category category);
    public void deleteCategory(long catId);
    public void listProductsOfCategory(long catId);
	public boolean existsCategory(long catId);
}
