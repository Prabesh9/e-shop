package com.esapmarka.eshop.domain.category;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

	@Override
	public Category addCategory(Category category) {
        return categoryRepository.save(category);
	}

	@Override
	public List<Category> listCategory() {
        return categoryRepository.findAll();
	}

	@Override
	public Optional<Category> categoryDetail(long catId) {
        return categoryRepository.findById(catId);
	}

	@Override
	public Category updateCategory(Category category) {
        return categoryRepository.save(category);
	}

	@Override
	public void deleteCategory(long catId) {
            categoryRepository.deleteById(catId);	
	}

	@Override
	public void listProductsOfCategory(long catId) {
		
	}

	@Override
	public boolean existsCategory(long catId) {
        Optional<Category> category = categoryDetail(catId);
        return category.isPresent();
	}

}
